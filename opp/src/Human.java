public class Human {
    String name;
    int age;

    {
        System.out.println("New Human created");
    }

    Human() {
        System.out.println("Constructor called");
    }

    Human(String name, int age) {
        this(name);
        this.age = age;
    }

    Human(String name) {
        this();
        this.name = name;
        this.age = 0;
    }

    static {
        System.out.println("Class human loaded");
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }
}


