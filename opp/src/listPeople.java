import java.util.*;

public class listPeople {

    public static void main(String[] args) {
        List<Human> peoples = new ArrayList<>();

        Scanner sc = new Scanner(System.in);
        String name;
        int age;
        while (true) {
            System.out.print("Введите имя: ");
            name = sc.next();
            if (Objects.equals(name.trim().toLowerCase(), "exit")) break;
            age = -1;
            while (age < 0) {
                System.out.print("Введите возраст: ");
                try {
                    age = sc.nextInt();

                } catch (InputMismatchException e) {
                    sc.next();
                }
                if (age < 0) System.out.println("введено не допустимое значение возраста");
            }
            peoples.add(new Human(name, age));
        }

        sc.close();
        for (Human people : peoples) {
            System.out.println("Hello my name is " + people.getName());
            System.out.println("Hello my age is " + people.getAge());
        }
    }
}