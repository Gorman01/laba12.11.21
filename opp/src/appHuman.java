public class appHuman {
    public static void main(String[] args) {
        Human ivan = new Human();
        ivan.name = "Ivan";
        ivan.age = 30;

        Human masha = new Human("Masha", 25);
        System.out.println(ivan);
        System.out.println(ivan.getName());
        System.out.println(masha.getName());

        Human igor = new Human("Igor");
        System.out.println(igor.name+ " "+ igor.age);
    }
}
